﻿using System;

namespace exercise__17
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           
           bool y;
           bool n;

            Console.WriteLine("Is the sky Blue?");
            Console.WriteLine("Type y for yes and n for no");
            Console.ReadLine();

            y = (true);
            n = (false);

            if( y )
                Console.WriteLine("Well Done");
            else
               
            if( n )
                    Console.WriteLine("You're crazy");



           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
